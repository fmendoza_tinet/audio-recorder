import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Media, MediaObject } from '@ionic-native/media'
import 'rxjs/add/operator/map';

export enum AudioRecorderState {
  Ready,
  Recording,
  Recorded,
  Playing
}

/*
  Generated class for the AudiorecorderProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AudiorecorderProvider {

  media: Media = null;
  audio: MediaObject = null;
  state: AudioRecorderState = AudioRecorderState.Ready;

  get MediaObject(): MediaObject {
    if(this.media == null && this.audio == null) {

      this.media = new Media();
      this.audio = this.media.create('miaudio.wav');
    }

    return this.audio;
  }

  constructor(public http: Http) {
    console.log('Hello AudiorecorderProvider Provider');
  }

    startRecording() {
    this.MediaObject.startRecord();
    this.state = AudioRecorderState.Recording;
  }

  stopRecording() {
    this.MediaObject.stopRecord();
    this.state = AudioRecorderState.Recorded;
  }

  startPlayback() {
    this.MediaObject.play();
    this.state = AudioRecorderState.Playing;
  }

  stopPlayback() {
    this.MediaObject.stop();
    this.state = AudioRecorderState.Ready;
  }
}
