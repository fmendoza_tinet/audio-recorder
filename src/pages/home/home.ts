import { AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AudiorecorderProvider, AudioRecorderState } from '../../providers/audiorecorder/audiorecorder';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  AudioRecorderState = AudioRecorderState;

  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public audioRecorder: AudiorecorderProvider) {
      console.log('HomePage constructor');
  }

  startRecording() {
    try {
      this.audioRecorder.startRecording();
    }
    catch (e) {
      console.log(e);
      this.showAlert('Could not start recording.');
    }
  }

  stopRecording() {
    try {
      this.audioRecorder.stopRecording();
    }
    catch (e) {
      console.log(e);
      this.showAlert('Could not stop recording.');
    }
  }

  startPlayback() {
    try {
      this.audioRecorder.startPlayback();
    }
    catch (e) {
      console.log(e);
      this.showAlert('Could not play recording.');
    }
  }

  stopPlayback() {
    try {
      this.audioRecorder.stopPlayback();
    }
    catch (e) {
      console.log(e);
      this.showAlert('Could not stop playing recording.');
    }
  }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}
